# Angular commands reminder

## New project

### Default

```bash
ng new [name] [options]
```

### Basic

```bash
ng new [name] --style=scss
```

### Routing

```bash
ng new [name] --style=scss --routing
```

### No tests

```bash
ng new [name] --style=scss --routing --interactive --skip-tests
```

### No install (if npm issues)

```bash
ng new [name] --style=scss --routing --interactive --skip-tests --commit=false --skip-install
```
